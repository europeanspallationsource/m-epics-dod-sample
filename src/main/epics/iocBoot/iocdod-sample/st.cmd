#!../../bin/linux-x86_64/dod-sample

< envPaths
epicsEnvSet("NELM",      "240000")
epicsEnvSet("PREFIX",    "DTL-Ctrl:DAQ1")
epicsEnvSet("IOCPREFIX", "IOC1")
epicsEnvSet("TRPREFIX",  "DTL-Ctrl:TR-01")
epicsEnvSet("TRPORT",    "TRPIPE1")
epicsEnvSet("SIMPORT",   "DODSAMPLE")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",   "2097152")

cd "${TOP}"


## Register all support components
dbLoadDatabase "dbd/dod-sample.dbd"
dod_sample_registerRecordDeviceDriver pdbbase

## Create timing receiver
ndsCreateDevice "ndsTr", "$(TRPORT)", "FILE=/dev/era3, HBEN=1, TG_SIMULATOR=1"

## Load TR record instances
dbLoadRecords "${TR}/db/tr.db",                "PREFIX=$(TRPREFIX), ASYN_PORT=$(TRPORT)"
dbLoadRecords "${TR}/db/trInterrupt.template", "PREFIX=$(TRPREFIX), ASYN_PORT=$(TRPORT), EVENT=5, DESTRECORD=$(IOCPREFIX):DODSET42"

## Load source simulations
dbLoadRecords "db/source-single.template", "PREFIX=$(PREFIX):BI0, NELM=1,       ASYN_PORT=$(SIMPORT), ADDR=0, TIMEOUT=1, BUFRECORD=$(PREFIX):BI0:DODBUF"
dbLoadRecords "db/source-wf.template",     "PREFIX=$(PREFIX):AI0, NELM=${NELM}, ASYN_PORT=$(SIMPORT), ADDR=0, TIMEOUT=1, BUFRECORD=$(PREFIX):AI0:DODBUF, TRPREFIX=$(TRPREFIX)"

## Load DTE record
dbLoadRecords "${DOD}/db/set.template", "PREFIX=$(PREFIX), IOCEVENT=42"

## Map set to PVs
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):BI0, IOCEVENT=42, NPRE=20, NPOST=20"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):AI0, IOCEVENT=42, NPRE=100, NPOST=100"

## Load DOD record instances
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):BI0, TYPE=FLOAT, NELM=1,       MAXNPRE=100, CLKEVNT=254, BUNCHSIZE=1000"
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):AI0, TYPE=FLOAT, NELM=${NELM}, MAXNPRE=100, CLKEVNT=254"

## initialize simulation record
simDataDriverConfigure(${NELM})

#asynSetTraceMask "DODSAMPLE" 0 0xffff

cd "${TOP}/iocBoot/${IOC}"

iocInit
