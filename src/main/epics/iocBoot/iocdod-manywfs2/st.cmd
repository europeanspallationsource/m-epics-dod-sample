#!../../bin/linux-x86_64/dod-sample

## You may have to change dod-sample to something else
## everywhere it appears in this file

< envPaths
epicsEnvSet("NELM", 100000)
epicsEnvSet("NPRE", 28)
epicsEnvSet("NPOST", 10)
epicsEnvSet("IOCPREFIX", "IOC")
epicsEnvSet("PREFIX", "DT3")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",   "2097152")
epicsEnvSet("ASYN_PORT", "WF")

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/dod-sample.dbd"
dod_sample_registerRecordDeviceDriver pdbbase

## Load MPS set
dbLoadRecords "${DOD}/db/set.template", "PREFIX=$(IOCPREFIX),   IOCEVENT=42, SET=MPS"

dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF00, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=0, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF01, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=1, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF02, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=2, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF03, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=3, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF04, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=4, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF05, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=5, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF06, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=6, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF07, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=7, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF08, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=8, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF09, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=9, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF10, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=0, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF11, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=1, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF12, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=2, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF13, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=3, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF14, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=4, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF15, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=5, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF16, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=6, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF17, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=7, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF18, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=8, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF19, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=9, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF20, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=0, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF21, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=1, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF22, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=2, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF23, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=3, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF24, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=4, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF25, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=5, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF26, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=6, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF27, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=7, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF28, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=8, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF29, ASYN_PORT=${ASYN_PORT}2, ASYN_ADDR=9, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF30, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=0, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF31, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=1, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF32, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=2, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF33, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=3, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF34, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=4, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF35, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=5, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF36, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=6, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF37, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=7, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF38, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=8, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF39, ASYN_PORT=${ASYN_PORT}3, ASYN_ADDR=9, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF40, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=0, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF41, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=1, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF42, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=2, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF43, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=3, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF44, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=4, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF45, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=5, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF46, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=6, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF47, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=7, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF48, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=8, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF49, ASYN_PORT=${ASYN_PORT}4, ASYN_ADDR=9, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS, CLKEVNT=20"

#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF00, ASYN_PORT=${ASYN_PORT}00, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF01, ASYN_PORT=${ASYN_PORT}01, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF02, ASYN_PORT=${ASYN_PORT}02, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF03, ASYN_PORT=${ASYN_PORT}03, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF04, ASYN_PORT=${ASYN_PORT}04, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF05, ASYN_PORT=${ASYN_PORT}05, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF06, ASYN_PORT=${ASYN_PORT}06, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF07, ASYN_PORT=${ASYN_PORT}07, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF08, ASYN_PORT=${ASYN_PORT}08, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF09, ASYN_PORT=${ASYN_PORT}09, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF10, ASYN_PORT=${ASYN_PORT}10, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF11, ASYN_PORT=${ASYN_PORT}11, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF12, ASYN_PORT=${ASYN_PORT}12, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF13, ASYN_PORT=${ASYN_PORT}13, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF14, ASYN_PORT=${ASYN_PORT}14, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF15, ASYN_PORT=${ASYN_PORT}15, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF16, ASYN_PORT=${ASYN_PORT}16, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF17, ASYN_PORT=${ASYN_PORT}17, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF18, ASYN_PORT=${ASYN_PORT}18, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF19, ASYN_PORT=${ASYN_PORT}19, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF20, ASYN_PORT=${ASYN_PORT}20, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF21, ASYN_PORT=${ASYN_PORT}21, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF22, ASYN_PORT=${ASYN_PORT}22, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF23, ASYN_PORT=${ASYN_PORT}23, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF24, ASYN_PORT=${ASYN_PORT}24, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF25, ASYN_PORT=${ASYN_PORT}25, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF26, ASYN_PORT=${ASYN_PORT}26, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF27, ASYN_PORT=${ASYN_PORT}27, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF28, ASYN_PORT=${ASYN_PORT}28, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF29, ASYN_PORT=${ASYN_PORT}29, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF30, ASYN_PORT=${ASYN_PORT}30, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF31, ASYN_PORT=${ASYN_PORT}31, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF32, ASYN_PORT=${ASYN_PORT}32, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF33, ASYN_PORT=${ASYN_PORT}33, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF34, ASYN_PORT=${ASYN_PORT}34, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF35, ASYN_PORT=${ASYN_PORT}35, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF36, ASYN_PORT=${ASYN_PORT}36, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF37, ASYN_PORT=${ASYN_PORT}37, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF38, ASYN_PORT=${ASYN_PORT}38, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF39, ASYN_PORT=${ASYN_PORT}39, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF40, ASYN_PORT=${ASYN_PORT}40, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF41, ASYN_PORT=${ASYN_PORT}41, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF42, ASYN_PORT=${ASYN_PORT}42, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF43, ASYN_PORT=${ASYN_PORT}43, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF44, ASYN_PORT=${ASYN_PORT}44, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF45, ASYN_PORT=${ASYN_PORT}45, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF46, ASYN_PORT=${ASYN_PORT}46, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF47, ASYN_PORT=${ASYN_PORT}47, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF48, ASYN_PORT=${ASYN_PORT}48, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF49, ASYN_PORT=${ASYN_PORT}49, NELM=${NELM}"

simDataDriverConfigure(${ASYN_PORT}0, ${NELM})
simDataDriverConfigure(${ASYN_PORT}1, ${NELM})
simDataDriverConfigure(${ASYN_PORT}2, ${NELM})
simDataDriverConfigure(${ASYN_PORT}3, ${NELM})
simDataDriverConfigure(${ASYN_PORT}4, ${NELM})

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncxxx,"user=ess-devHost"

#dbpf DT3:WF00:DODBUF.G 1
