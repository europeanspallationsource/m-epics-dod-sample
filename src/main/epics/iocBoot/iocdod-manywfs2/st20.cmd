#!../../bin/linux-x86_64/dod-sample

## You may have to change dod-sample to something else
## everywhere it appears in this file

< envPaths
epicsEnvSet("NELM", 240000)
epicsEnvSet("NPRE", 28)
epicsEnvSet("NPOST", 10)
epicsEnvSet("IOCPREFIX", "IOC")
epicsEnvSet("PREFIX", "DT3")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",   "2097152")
epicsEnvSet("ASYN_PORT", "WF")

cd ${TOP}

## Register all support components
dbLoadDatabase "dbd/dod-sample.dbd"
dod_sample_registerRecordDeviceDriver pdbbase

## Load MPS set
dbLoadRecords "${DOD}/db/set.template", "PREFIX=$(IOCPREFIX),   IOCEVENT=42, SET=MPS"

dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF00, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=0, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF01, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=1, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF02, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=2, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF03, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=3, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF04, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=4, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF05, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=5, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF06, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=6, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF07, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=7, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF08, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=8, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF09, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=9, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF10, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=0, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF11, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=1, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF12, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=2, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF13, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=3, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF14, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=4, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF15, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=5, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF16, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=6, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF17, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=7, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF18, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=8, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"
dbLoadRecords "db/manywfs2-single.db", "PREFIX=$(PREFIX):WF19, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=9, MAXNPRE=${NPRE}, NPRE=${NPRE}, NPOST=${NPOST}, NELM=${NELM}, TYPE=FLOAT, IOCEVENT=42, SET=MPS"




#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF00, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=0, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF01, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=1, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF02, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=2, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF03, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=3, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF04, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=4, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF05, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=5, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF06, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=6, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF07, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=7, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF08, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=8, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF09, ASYN_PORT=${ASYN_PORT}0, ASYN_ADDR=9, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF10, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=0, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF11, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=1, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF12, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=2, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF13, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=3, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF14, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=4, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF15, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=5, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF16, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=6, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF17, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=7, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF18, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=8, NELM=${NELM}"
#dbLoadRecords "db/source-manywfs2.template", "PREFIX=$(PREFIX):WF19, ASYN_PORT=${ASYN_PORT}1, ASYN_ADDR=9, NELM=${NELM}"

simDataDriverConfigure(${ASYN_PORT}0, ${NELM})
simDataDriverConfigure(${ASYN_PORT}1, ${NELM})

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncxxx,"user=ess-devHost"

#dbpf DT3:WF00:DODBUF.G 1
