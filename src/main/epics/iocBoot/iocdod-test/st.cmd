#!../../bin/linux-x86_64/dod-sample

< envPaths
epicsEnvSet("NELM", 240000)
epicsEnvSet("MPSNPRE", 10)
epicsEnvSet("MPSNPOST", 10)
epicsEnvSet("NPRE", 20)
epicsEnvSet("NPOST", 20)
epicsEnvSet("MAXNPRE", 50)
epicsEnvSet("IOCPREFIX", "IOC")
epicsEnvSet("PREFIX", "DT2")
epicsEnvSet("CLKEVENT", 100)
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",   "2097152")

cd "${TOP}"

dbLoadDatabase "dbd/dod-sample.dbd"
dod_sample_registerRecordDeviceDriver pdbbase

## Load sources
dbLoadRecords "db/source-test.template", "PREFIX=$(PREFIX), BUFRECORD=DODBUF, NELM=${NELM}"

## Load MPS set
dbLoadRecords "${DOD}/db/set.template", "PREFIX=$(IOCPREFIX),             IOCEVENT=42, SET=MPS"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-AI,         IOCEVENT=42, SET=MPS, NPRE=$(MPSNPRE), NPOST=$(MPSNPOST)"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-BI,         IOCEVENT=42, SET=MPS, NPRE=$(MPSNPRE), NPOST=$(MPSNPOST)"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-LONGIN,     IOCEVENT=42, SET=MPS, NPRE=$(MPSNPRE), NPOST=$(MPSNPOST)"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-MBBI,       IOCEVENT=42, SET=MPS, NPRE=$(MPSNPRE), NPOST=$(MPSNPOST)"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-C, IOCEVENT=42, SET=MPS, NPRE=$(MPSNPRE), NPOST=$(MPSNPOST)"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-F, IOCEVENT=42, SET=MPS, NPRE=$(MPSNPRE), NPOST=$(MPSNPOST)"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-D, IOCEVENT=42, SET=MPS, NPRE=$(MPSNPRE), NPOST=$(MPSNPOST)"

## Load set 1
dbLoadRecords "${DOD}/db/set.template", "PREFIX=$(IOCPREFIX),         IOCEVENT=110"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-AI,     IOCEVENT=110, NPRE=${NPRE}, NPOST=${NPOST}"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-BI,     IOCEVENT=110, NPRE=${NPRE}, NPOST=${NPOST}"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-LONGIN, IOCEVENT=110, NPRE=${NPRE}, NPOST=${NPOST}"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-MBBI,   IOCEVENT=110, NPRE=${NPRE}, NPOST=${NPOST}"

## Load set 2
dbLoadRecords "${DOD}/db/set.template", "PREFIX=$(IOCPREFIX),             IOCEVENT=99"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-C, IOCEVENT=99, NPRE=${NPRE}, NPOST=${NPOST}"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-F, IOCEVENT=99, NPRE=${NPRE}, NPOST=${NPOST}"
dbLoadRecords "${DOD}/db/map.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-D, IOCEVENT=99, NPRE=${NPRE}, NPOST=${NPOST}"

## Load DOD record instances
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):DEV-AI,         MAXNPRE=${MAXNPRE}, CLKEVNT=${CLKEVENT}, NELM=1,       TYPE=FLOAT, BUNCHSIZE=50"
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):DEV-BI,         MAXNPRE=${MAXNPRE}, CLKEVNT=${CLKEVENT}, NELM=1,       TYPE=UCHAR, BUNCHSIZE=50"
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):DEV-LONGIN,     MAXNPRE=${MAXNPRE}, CLKEVNT=${CLKEVENT}, NELM=1,       TYPE=LONG,  BUNCHSIZE=50"
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):DEV-MBBI,       MAXNPRE=${MAXNPRE}, CLKEVNT=${CLKEVENT}, NELM=1,       TYPE=UCHAR, BUNCHSIZE=50"
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-C, MAXNPRE=${MAXNPRE}, CLKEVNT=${CLKEVENT}, NELM=${NELM}, TYPE=CHAR"
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-F, MAXNPRE=${MAXNPRE}, CLKEVNT=${CLKEVENT}, NELM=${NELM}, TYPE=FLOAT"
dbLoadRecords "${DOD}/db/dod.template", "PREFIX=$(PREFIX):DEV-WAVEFORM-D, MAXNPRE=${MAXNPRE}, CLKEVNT=${CLKEVENT}, NELM=${NELM}, TYPE=DOUBLE"


cd "${TOP}/iocBoot/${IOC}"

iocInit

dbpf DT2:DEV-AI:DODBUF.G 1
