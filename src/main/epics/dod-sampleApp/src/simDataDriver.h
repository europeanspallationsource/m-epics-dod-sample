/*
 * simDataDriver.h
 *
 */

#ifndef SIMDATADRIVER_H
#define SIMDATADRIVER_H 1

#include "asynPortDriver.h"

/* These are the drvInfo strings that are used to identify the parameters.
 * They are used by asyn clients, including standard asyn device support */
#define P_AnalogString         "ANALOG"         /* asynFloat32Array,     r/w */
#define P_SingleString         "SINGLE"
#define P_ClockString         "CLOCK"

class simDataDriver : public asynPortDriver {
    public:
        /* Constructor */
        simDataDriver(const char *portName, int nsamples);

        /* Not inherited from asynPortDriver */
        void simTask(void);
        void simClk(void);

    protected:
        /** Values used for pasynUser->reason, and indexes into the parameter library. */
        int P_Analog;
        int P_Single;
        int P_Clock;

    private:
        /* Our data */
        epicsFloat32 *analogDataIn;
        int nsamples;
        int clock;
};

#endif /* simDataDriver.h */
/* vim: set sw=4 sts=4: */
