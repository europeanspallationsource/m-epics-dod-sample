/*
 * simDataDriver.cpp
 *
 * Intended to be used with DoD as a sample implementation.
 *
 * Author: Niklas Claesson <niklas.claesson@cosylab.com>
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>

#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <iocsh.h>

#include "simDataDriver.h"
#include <epicsExport.h>


static const char *driverName="simDataDriver";
void simTask(void *drvPvt);
void simClk(void *drvPvt);


/** Constructor for the simDataDriver class.
 * Calls constructor for the asynPortDriver base class.
 */
simDataDriver::simDataDriver(const char *portName, int nsamples_in)
    : asynPortDriver(portName,
            11, /* maxAddr */
            3, /* Number of params */
            asynInt32Mask | asynFloat32ArrayMask | asynDrvUserMask, /* Interface mask */
            asynInt32Mask | asynFloat32ArrayMask,  /* Interrupt mask */
            ASYN_CANBLOCK, /* asynFlags. */
            1, /* Autoconnect */
            0, /* Default priority */
            0) /* Default stack size*/
{
    asynStatus status;

    /* Initiate data */
    nsamples = nsamples_in;
    analogDataIn = (epicsFloat32 *)calloc(nsamples, sizeof(epicsFloat32));
    //printf("Calloc %d %lu %f\n", nsamples, sizeof(epicsFloat32), nsamples * sizeof(epicsFloat32) / 1024.0 / 1024.0);
    if(analogDataIn == NULL) {
        printf("%s: Calloc failed, you're out of memory...\n", driverName);
    }

    /* Create Param */
    createParam(P_SingleString, asynParamInt32,        &P_Single);
    createParam(P_AnalogString, asynParamFloat32Array, &P_Analog);

    createParam(P_ClockString, asynParamInt32, &P_Clock);

    status = (asynStatus)(epicsThreadCreate("simDataDriverTask2",
                epicsThreadPriorityMedium,
                epicsThreadGetStackSize(epicsThreadStackMedium),
                (EPICSTHREADFUNC)::simClk,
                this) == NULL);
    if (status) {
        printf("%s: epicsThreadCreate failure\n", driverName);
        return;
    }
    /* Create the thread that throws timer interrupts */
    status = (asynStatus)(epicsThreadCreate("simDataDriverTask1",
                epicsThreadPriorityMedium,
                epicsThreadGetStackSize(epicsThreadStackMedium),
                (EPICSTHREADFUNC)::simTask,
                this) == NULL);
    if (status) {
        printf("%s: epicsThreadCreate failure\n", driverName);
        return;
    }
}

void simTask(void *drvPvt)
{
    simDataDriver *pPvt = (simDataDriver *)drvPvt;

    pPvt->simTask();
}

void simClk(void *drvPvt)
{
    simDataDriver *pPvt = (simDataDriver *)drvPvt;

    pPvt->simClk();
}

inline unsigned int random(unsigned int last)
{
    unsigned int a = 1103515245;
    unsigned int c = 12345;

    return (last*a + c) % RAND_MAX;
}

void simDataDriver::simClk(void)
{
    epicsTimeStamp pre, post;
    double sleepTime;

    epicsThreadSleep(2);

    /* Loop forever */
    while (1) {
        epicsTimeGetCurrent(&pre);
        clock++;
        setIntegerParam(P_Clock, clock);
        callParamCallbacks();
        epicsTimeGetCurrent(&post);

        /* We want new values with 14 Hz */
        sleepTime = 1/(double)14 - epicsTimeDiffInSeconds(&post,&pre);
        //sleepTime = 1/(double)14 - epicsTimeDiffInSeconds(&post,&pre);

        if(sleepTime > 0)
            epicsThreadSleep(sleepTime);
        else
            printf("%s simClk: Took to long to generate data (%.7f)\n", driverName, -sleepTime);
    }

}

/** Simulation task that runs as a separate thread. */
void simDataDriver::simTask(void)
{
    int i;
    epicsTimeStamp pre, post;
    double sleepTime;
    float tmp[4];

    for(i=0;i<4;++i) tmp[i] = rand()/(float)RAND_MAX;

    for(i=0;i<nsamples/4;++i){
        analogDataIn[4*i+0] = tmp[0];
        analogDataIn[4*i+1] = tmp[1];
        analogDataIn[4*i+2] = tmp[2];
        analogDataIn[4*i+3] = tmp[3];
    }

    epicsThreadSleep(2);

    /* Loop forever */
    while (1) {
        epicsTimeGetCurrent(&pre);

#if 1
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 0);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 1);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 2);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 3);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 4);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 5);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 6);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 7);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 8);
        doCallbacksFloat32Array(analogDataIn, nsamples, P_Analog, 9);
#endif

#if 0
        char tmpbuf[20];
        epicsTimeToStrftime(tmpbuf,20,"%M:%S.%06f",&pre);
        printf("Created: %s data=%.6f\n", tmpbuf, analogDataIn[0]);
#endif
        epicsTimeGetCurrent(&post);
        /* We want new values with 14 Hz */
        sleepTime = 1/(double)14 - epicsTimeDiffInSeconds(&post,&pre);
        //sleepTime = 1/(double)14 - epicsTimeDiffInSeconds(&post,&pre);

        if(sleepTime > 0)
            epicsThreadSleep(sleepTime);
        else
            printf("%s simTask: Took to long to generate data (%.7f)\n", driverName, -sleepTime);
    }
}


/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

    /** EPICS iocsh callable function to call constructor for the simDataDriver class.*/
    int simDataDriverConfigure(const char*portName, int nsamples)
    {
        new simDataDriver(portName, nsamples);
        return(asynSuccess);
    }


    /* EPICS iocsh shell commands */
    static const iocshArg initArg0 = { "portName", iocshArgString};
    static const iocshArg initArg1 = { "nsamples", iocshArgInt};
    static const iocshArg * const initArgs[] = {&initArg0, &initArg1};
    static const iocshFuncDef initFuncDef = {"simDataDriverConfigure",2,initArgs};
    static void initCallFunc(const iocshArgBuf *args)
    {
        simDataDriverConfigure(args[0].sval, args[1].ival);
    }

    void simDataDriverRegister(void)
    {
        iocshRegister(&initFuncDef,initCallFunc);
    }

    epicsExportRegistrar(simDataDriverRegister);

}

/* vim: set sw=4 sts=4: */
