#include <stdio.h>
#include <string.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <epicsTime.h>
#include <cantProceed.h>

unsigned int rep;  /* Repition of anomaly */

static long badWfInit(aSubRecord *precord)
{
	epicsUInt8 *bypass; /* Bypass filter (1 enable, 0 disable) */

	bypass = (epicsUInt8 *) precord->b;

	*bypass = 1;
	rep = 10;

	return 0;
}

static long badWfProcess(aSubRecord *precord)
{
	epicsFloat32 *dataIn;     /* Incoming data array */
	epicsFloat32 *dataOutSrc; /* Outgoing data array */
	epicsFloat32 *dataOutAno; /* Outgoing data array */
	epicsUInt32   nElements;  /* Elements in data arrays */
	epicsUInt8   *bypass;     /* Bypass filter (1 enable, 0 disable) */
	unsigned int  start;      /* Start of anomaly */
	unsigned int  length;     /* Length of anomaly */
	int i;

	dataIn     = (epicsFloat32 *) precord->a;
	bypass     =   (epicsUInt8 *) precord->b;
	dataOutSrc = (epicsFloat32 *) precord->vala;
	dataOutAno = (epicsFloat32 *) precord->valb;
	nElements  =                  precord->noa;

	/* If bypass, don't do anything */
	if(*bypass) {
		memcpy(dataOutSrc, dataIn, nElements * sizeof(epicsFloat32));
		memcpy(dataOutAno, dataIn, nElements * sizeof(epicsFloat32));
		return 0;
	}

	/* Randomize start and length of anomaly.
	 * Start can be anywhere in the middle third. Length can be up to a
	 * tenth of total waveform. */
	start  = (unsigned int) nElements/3 + (rand()/(float)RAND_MAX)*(nElements/3);
	length = (unsigned int) ((rand()/(float)RAND_MAX)*(nElements/10));
	
	printf("(%s) start: %u length: %u repitition: %u\n", __func__, start, length, rep);

	for(i = 0; i < nElements; ++i) {
		/* If anomaly, reduce signal strength with 20 */
		if(i >= start && i-start < length) {
			dataOutSrc[i] = dataIn[i]/20;
			dataOutAno[i] = dataIn[i]/20;
		} else {
			dataOutSrc[i] = dataIn[i];
			dataOutAno[i] = dataIn[i];
		}
	}

	/* Decrease rep and reset bypass when bottom is reached */
	if(--rep == 0) {
		*bypass = 1;
		rep = 10;
	}
	
	return 0;
}

epicsRegisterFunction(badWfInit);
epicsRegisterFunction(badWfProcess);
