/*!
 * \file readCounter.c
 * \author Niklas Claesson <niklas.claesson@cosylab.com>
 */
#include <stdio.h>
#include <string.h>

#include <dbDefs.h>
#include <dbAccess.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <epicsTime.h>
#include <cantProceed.h>
#include <errlog.h>
#include <recGbl.h>
#include <alarm.h>

#define DEBUG 0
#define TRACE 0

#define debug_print(fmt, ...) \
            do { if (*(epicsInt32 *) precord->g) fprintf(stderr, "[D] %25s %15s(): " fmt, precord->name, __func__, __VA_ARGS__); } while (0)
#define debug2_print(fmt, ...) \
            do { if (DEBUG > 1) fprintf(stderr, "[D] %25s %15s(): " fmt, precord->name, __func__, __VA_ARGS__); } while (0)

#define trace_print() \
            do { if (TRACE) fprintf(stderr, "[T] %25s %15s()\n", precord->name, __func__); } while (0)


static long readCounterProcess(aSubRecord *precord)
{
        int *priv;        /* Application specific data */

        priv        = (int *) precord->vala;

	(*priv)++;

        return 0;
}

/* Register these symbols for use by IOC code: */
epicsRegisterFunction(readCounterProcess);
