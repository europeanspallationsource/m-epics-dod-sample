#include <stdio.h>
#include <string.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <epicsTime.h>
#include <cantProceed.h>

#define THRESHOLD_AMP 1     /* Threshold amplitude */
#define THRESHOLD_LEN 100 /* Threshold length of beam loss */

static long anomalyDetectProcess(aSubRecord *precord)
{
	epicsFloat32 *dataIn;    /* Incoming data array */
	epicsFloat32 *dataOut;   /* Outgoing data array */
	epicsUInt32   nElements; /* Elements in data arrays */
	epicsInt32   *asynConf;  /* Configuration for Asynchronous timing event */
	char         *asynMsgs;  /* Message receiver for async timing event */
	unsigned int  length;    /* Length of anomaly */
	int           i;

	dataIn    = (epicsFloat32 *) precord->a;
	dataOut   = (epicsFloat32 *) precord->vala;
	asynConf  =   (epicsInt32 *) precord->valb;
	asynMsgs  =         (char *) precord->valc;
	nElements =                  precord->noa;
	length	  =                  0;

	/* Pass through the data */
	memcpy(dataOut, dataIn, nElements * sizeof(epicsFloat32));


	for(i = 0; i < nElements; ++i) {
		if(dataIn[i] < THRESHOLD_AMP)
			++length;
	}
	
	asynConf[1] = 0; /* Trigger timing event */
	asynConf[2] = 0; /* Delay after trigger (unit: 10*Tevent) */
	asynConf[3] = 0; /* Number of repitions */
	asynConf[4] = 0; /* Mode 0: single, 1: continuous */

	if(length > THRESHOLD_LEN) {
		printf("(%s) Trigger %d\n",__func__, length);
		/* Timing event to be emitted */
		asynConf[0] = 5;
		strcpy(asynMsgs, "START");
	} else {
		/* Timing event to be emitted */
		asynConf[0] = 0;
		strcpy(asynMsgs, "");
	}
	
	return 0;
}

epicsRegisterFunction(anomalyDetectProcess);
