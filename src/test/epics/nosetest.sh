#!/bin/bash

nosetests ~/git/m-epics-dod-sample/src/test/epics/python/test.py -v -a '!slow' --with-xunit -x --with-ignore-docstring
