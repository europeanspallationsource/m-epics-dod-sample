#!/bin/bash
mvn run -Dioc=dod-test -Ddaemon=true

export EPICS_CA_ADDR_LIST="localhost"
export EPICS_CA_AUTO_ADDR_LIST="NO"

python/test.py $1
RES=$?
mvn stop -Dioc=dod-test
exit $RES
