# This is a python module containing classes useful for testing of
# Data-On-Demand.

import time
from epics import PV,dbr
import numpy.random as nprnd
import numpy as np
from datetime import datetime, timedelta
import subprocess
from decimal import *

class Source(object):
    def __init__(self, source, nelm, prefix):
        self._prefix  = "{0}:{1}".format(prefix,source)
        self._nelm    = nelm
        self._pvs     = {}
        self._static_sample = self.get_random()
        self.open_logs()
        self._bunches = {'ts': {'s':[],'ns':[]}, 'tsms': [],'val': []}
        self._firstArcChange = False
        self._firstBunchTsSChange = False
        self._firstBunchTsNSChange = False
        self._firstBunchTsMSChange = False
        self._firstBunchValChange = False
        self._timeout = 1
        if nelm == 1:
            prio = 30
            self._pvs['bunchval'] = PV('{0}:DODBUNCHVAL'.format(self._prefix),
                callback=self.onBunchValChanges, auto_monitor=True, connection_timeout=self._timeout)
            self._pvs['bunchts-s']  = PV('{0}:DODBUNCHTSS'.format(self._prefix),
                callback=self.onBunchTsSChanges, auto_monitor=True, connection_timeout=self._timeout)
            self._pvs['bunchts-ns']  = PV('{0}:DODBUNCHTSNS'.format(self._prefix),
                callback=self.onBunchTsNSChanges, auto_monitor=True, connection_timeout=self._timeout)
            self._pvs['bunchts-ms']  = PV('{0}:DODBUNCHTSX'.format(self._prefix),
                callback=self.onBunchTsMSChanges, auto_monitor=True, connection_timeout=self._timeout)
        else:
            prio = 0

        # Find PVS
        self._pvs['src']     = PV('{0}'.format(self._prefix), priority=prio, connection_timeout=self._timeout)
        self._pvs['arc']     = PV('{0}:DOD'.format(self._prefix), connection_timeout=self._timeout,
                callback=self.onArcChanges, priority=prio, auto_monitor=True, form='time')
        self._pvs['mnpre']    = PV('{0}:DODMAXNPRE'.format(self._prefix), connection_timeout=self._timeout)
        self._pvs['clk']     = PV('{0}:DODCLK.PROC'.format(self._prefix), connection_timeout=self._timeout)

        timeout = 0
        while not self.connected():
            time.sleep(1.e-2)
            timeout = timeout + 1
            if timeout == 500:
                print "[EE] Failed to connect to PV {0}".format(self._prefix)
                break

    def open_logs(self):
        """Opens the logfiles which are compared in the end"""
        self._src_log = open('src-{0}.log'.format(self._prefix), 'w+')
        self._arc_log = open('arc-{0}.log'.format(self._prefix), 'w+')
        if self._nelm == 1:
            self._bunch_log= open('bunch-{0}.log'.format(self._prefix), 'w+')

    def close_logs(self):
        """Closes all opened files"""
        if self._src_log:
            self._src_log.close()
        if self._arc_log:
            self._arc_log.close()
        if self._nelm == 1 and self._bunch_log:
            self._bunch_log.close()

    def disconnect(self):
        """Disconnect all PVs in source"""
        for (key,pv) in self._pvs.iteritems():
            pv.disconnect()

    def finalize_bunches(self):
        """Write bunches to a logfile so that it can be compared to the source"""
        for (ts, nsec, val) in zip(self._bunches['ts']['s'], self._bunches['ts']['ns'], self._bunches['val']):
            stamp = Timestamp(ts, nsec)
            date = datetime.fromtimestamp(dbr.make_unixtime(stamp))
            self._bunch_log.write(self.format_output(date, val))


    def verify_multi(self, size, continuous):
        """Verification method for multiple triggers"""
        if self._nelm == 1:
            self.finalize_bunches()
            res =  self._verify_multi(self._src_log, self._arc_log, size, continuous) and \
                    self._verify_multi(self._src_log, self._bunch_log, size, continuous)
        else:
            res = self._verify_multi(self._src_log, self._arc_log, size, continuous)
        self.close_logs()
        return res

    def verify(self, npre, npost, dte_ts):
        """Basic verification method"""
        if self._nelm == 1:
            self.finalize_bunches()
            res = self._verify(self._src_log, self._arc_log, npre, npost, dte_ts) and \
                    self._verify(self._src_log, self._bunch_log, npre, npost, dte_ts)
        else:
            res = self._verify(self._src_log, self._arc_log, npre, npost, dte_ts)
        self.close_logs()
        return res

    def _verify(self, src, arc, npre, npost, dte_ts):
        """Compares src and arc line by line. Also checks the number of values
        before the dte timestamp and after.
        """
        src.seek(0)
        arc.seek(0)

        first_arc_line = arc.readline()

        state = "init"
        pre_c = 0
        post_c = 0
        # State machine, it iterates over all the lines in the source-file
        # 
        # It is in 'init' until it find the first occurance of the first
        # archived line. When it moves to 'first_found'.
        #
        # It stays in 'first_found' until the datetime is larger than the
        # datetime of the timing event.
        for line in src:
            if state == "init":
                if line == first_arc_line:
                    state = "first_found"
                    pre_c = pre_c + 1
            elif state == "first_found":
                tmp = arc.readline()
                if tmp == "":
                    break
                ts = datetime.strptime(' '.join(tmp.split(' ')[1:3]), "%Y-%m-%d %H:%M:%S.%f" )
                if line != tmp:
                    print '[EE] src:', line.rstrip('\n'), "!=\n[EE] arc:", tmp.rstrip('\n')
                    state = "error"
                elif ts > dte_ts:
                    state = "post"
                    post_c  = post_c + 1
                else:
                    pre_c   = pre_c + 1
            elif state == "post":
                tmp = arc.readline()
                if tmp == "":
                    break
                elif line != tmp:
                    print '[EE] src:', line.rstrip('\n'), "!=\n[EE] arc:", tmp.rstrip('\n')
                    state = "error"
                else:
                    post_c  = post_c + 1

        if pre_c < npre or post_c < npost:
            print '[EE] Not enough archived values found'
            state = "error"

        if pre_c > npre or post_c > npost:
            print '[EE] Too many archived values found'
            state = "error"

        if state == "error":
            print "[EE] {0:22s} [{1:2}:{2:2}] {3:2} of {4}   [FAILED]".format(self._prefix, pre_c, post_c, pre_c+post_c, npre+npost)
            return False
        else:
            print "[II] {0:25s} {1:2} {2:2}               [  OK  ]".format(self._prefix, pre_c, post_c)
        return True

    def _verify_multi(self, src, arc, size, continuous):
        """Compares src and arc line by line and counts the number of lines.
        If 'continuous == True' it will not accept any gaps in the data.
        """
        src.seek(0)
        arc.seek(0)

        first_arc_line = arc.readline()

        state = "init"
        matching_c = 0
        for line in src:
            if state == "init":
                if line == first_arc_line:
                    state = "first_found"
                    matching_c = matching_c + 1
            elif state == "first_found":
                tmp = arc.readline()
                if tmp == "":
                    break
                if line != tmp and continuous:
                    print '[EE] src:', line.rstrip('\n'), "!=\n[EE] arc:", tmp.rstrip('\n')
                    state = "error"
                else:
                    matching_c   = matching_c + 1

        if matching_c < size:
            print '[EE] Not enough archived values found'
            state = "error"

        if matching_c > size:
            print '[EE] Too many archived values found'
            state = "error"

        if state == "error":
            print "[EE] {0:22s} {1:2} of {2:2}   [FAILED]".format(self._prefix, matching_c, size)
            return False
        else:
            print "[II] {0:25s} {1:2} of {2:2}   [  OK  ]".format(self._prefix, matching_c, size)
        return True

    def connected(self):
        """Returns true of all pvs are connected"""
        return all([ pv.wait_for_connection() for (name,pv) in self._pvs.iteritems() ])

    def onArcChanges(self, pvname=None, value=None, timestamp=None, **kw):
        if not self._firstArcChange:
            self._firstArcChange = True
        else:
            date = datetime.fromtimestamp(timestamp)
            self._arc_log.write(self.format_output(date, value))

    def onBunchTsSChanges(self, pvname=None, value=None, **kw):
        if not self._firstBunchTsSChange:
            self._firstBunchTsSChange = True
        else:
            for s in value:
                self._bunches['ts']['s'].append(s)

    def onBunchTsNSChanges(self, pvname=None, value=None, **kw):
        if not self._firstBunchTsNSChange:
            self._firstBunchTsNSChange = True
        else:
            for ns in value:
                self._bunches['ts']['ns'].append(ns)

    def onBunchTsMSChanges(self, pvname=None, value=None, **kw):
        if not self._firstBunchTsMSChange:
            self._firstBunchTsMSChange = True
        else:
            for ts in value:
                self._bunches['tsms'].append(Decimal('{0:13.3f}'.format(ts)))

    def onBunchValChanges(self, pvname=None, value=None, **kw):
        if not self._firstBunchValChange:
            self._firstBunchValChange = True
        else:
            for v in value:
                self._bunches['val'].append(v)

    def clock_complete(self):
        """Returns true when the readout has been completed.
        Used to get more fine grained control over what to do while waiting
        for the clock lock set to process.
        """
        return self._pvs['clk'].put_complete

    def put_random(self):
        """Put a random value in the PV. Write the value with the correct
        timestamp to the logfile.
        """
        src = self._pvs['src']
        val = self.get_random()
        src.put(val, wait=True)
        timestamp = src.get_timevars()['timestamp']
        date = datetime.fromtimestamp(timestamp)
        self._src_log.write(self.format_output(date, val))

    def put_static(self):
        """Put a static value in the PV. Write the value with the correct
        timestamp to the logfile.
        """
        src = self._pvs['src']
        src.put(self._static_sample, wait=True)
        timestamp = src.get_timevars()['timestamp']
        date = datetime.fromtimestamp(timestamp)
        self._src_log.write(self.format_output(date, self._static_sample))

    def clock(self):
        """Pop the DOD buffer once.
        Wait until 'clock_complete()' returns True before continuing.
        """
        self._pvs['clk'].put(1, use_complete=True)

    @property
    def mnpre(self):
        """Get DOD buffer length"""
        return self._pvs['mnpre'].get()

    @mnpre.setter
    def mnpre(self, value):
        """Set DOD buffer length"""
        self._pvs['mnpre'].put(value, wait=True)

class WaveformDoubleSource(Source):
    def format_output(self, date, value):
        res = "{0} {1} {2} ".format(self._prefix, date, self._nelm)
        res = res + " ".join(["{0:.14f}".format(float(x)) for x in np.concatenate((value[0:9],value[-10:-1]))])
        return res + "\n"

    def get_random(self):
        # Return an array of a single random double value
        return np.full(self._nelm, nprnd.random())

class WaveformFloatSource(Source):
    def format_output(self, date, value):
        res = "{0} {1} {2} ".format(self._prefix, date, self._nelm)
        res = res + " ".join(["{0:.6f}".format(float(x)) for x in np.concatenate((value[0:9],value[-10:-1]))])
        return res + "\n"

    def get_random(self):
        # Return an array of a single random float value
        return np.full(self._nelm, round(nprnd.random(),6))

class WaveformCharSource(Source):
    def format_output(self, date, value):
        res = "{0} {1} {2} ".format(self._prefix, date, self._nelm)
        res = res + " ".join(map(str,np.concatenate((value[0:9],value[-10:-1]))))
        return res + "\n"

    def get_random(self):
        # Return an array of a single random char value
        return np.full(self._nelm, int((2**7)*nprnd.random()), dtype=int)

class AiSource(Source):
    def format_output(self, date, value):
        return "{0} {1} {2:.6f}\n".format(self._prefix, date, float(value), value)

    def get_random(self):
        return round(nprnd.random(self._nelm)[0],6)

class BiSource(Source):
    def format_output(self, date, value):
        val = round(value % 2)
        return "{0} {1} {2}\n".format(self._prefix, date, val)

    def get_random(self):
        return round(nprnd.random(self._nelm))

class LonginSource(Source):
    def format_output(self, date, value):
        val = value % 2**31
        return "{0} {1} {2}\n".format(self._prefix, date, val)

    def get_random(self):
        return int((2**31)*nprnd.random(self._nelm))

class MbbiSource(Source):
    def format_output(self, date, value):
        val = value % 16
        return "{0} {1} {2}\n".format(self._prefix, date, val)

    def get_random(self):
        return int((2**15)*nprnd.random(self._nelm))

class Timestamp(object):
    def __init__(self, secs, nsec):
        self.secs=secs
        self.nsec=nsec
