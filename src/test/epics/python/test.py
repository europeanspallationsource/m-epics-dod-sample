#!/usr/bin/python
from epics import PV # Depend on version 3.2.3
from epics import ca
import os, sys
from optparse import OptionParser
import time
from source import *
from sourceset import SourceSet
import threading
import random
from subprocess import call
import nose
from nose.plugins.attrib import attr
from nose.plugins.skip import SkipTest
os.putenv('EPICS_CA_MAX_ARRAY_BYTES','2097152')



def set_nds_state(pv, pv_rbv, state):
    pv.put(state, wait=True)
    if pv_rbv.get() != state:
        print "Could not set {0} to {1}".format(pv, state)

class GenericIOC(object):
    """This class handles an IOC. All test classes extend this class. It defines
    setUp and tearDown methods.
    """
    def __init__(self, prefix, ioc, rand):
        self._prefix = prefix
        self._ioc = ioc
        self._rand = rand
        self._failed = False

    def setUp(self):
        """Nose creates all objects before running any tests. Therefore the IOC has
        to be started in the setUp.
        """
        process = '{0}-ioc fg'.format(self._ioc)
        self.iocoutlog = open('ioc-out.log','w+')
        self.iocerrlog = open('ioc-err.log','w+')
        self._iocProc = subprocess.Popen(process, stdin=subprocess.PIPE, stdout=self.iocoutlog, stderr=self.iocerrlog, shell=True)
        self.connect_pvs()

    def tearDown(self):
        """The tearDown function disconnects all PVs and flushes all CA connections.
        If the test failes, it will write the ioc stderr and stdout to log files.
        Finally it kills the IOC.
        """
        #time.sleep(1)
        [ src.disconnect() for src in self._sources ]
        [ set.disconnect() for set in self._sets ]
        ca.finalize_libca(2)
        self._iocProc.communicate("exit\n")
        self.iocoutlog.close()
        self.iocerrlog.close()

@attr('tr')
class TestDodWithTR(GenericIOC):
    def __init__(self):
        raise SkipTest()
        super(TestDodWithTR, self).__init__("DT1", "dod-sample-ioc", True)
        self.init_tr()

    def init_tr(self):
        self._pvs['msgs'] = PV('DTL-Ctrl:TR-01:MSGS')
        self._pvs['rbv']  = PV('DTL-Ctrl:TR-01')
        self._pvs['filt_dist']  = PV('{0}:DEV-WF-FILTDIST.B'.format(self._prefix))
        set_nds_state(self._pvs['msgs'], self._pvs['rbv'], 'INIT')
        set_nds_state(self._pvs['msgs'], self._pvs['rbv'], 'ON')

    def trigger(self):
        self._pvs['filt_dist'].put(0, wait=True)

    def testStandalone(self):
        time.sleep(1)
        self.requestMPS()
        tim.sleep(1)
        return self.verify(10,10)

class DodBase(GenericIOC):
    """This Class contains most tests and implements generaton of data and clocking
    out of data. It is extended by the "Fast" test and the "Slow" test.

    * The fast test testes only a scalar PV.
    * The slow test testes many types of data such as int, float, array of ints or floats.
    """
    def __init__(self, prefix, ioc, rand):
        super(DodBase, self).__init__(prefix, ioc, rand)

    def gen_sim_data(self, amount, gen_random=None, clock='post'):
        """Generates raw data and a readout. The reading can be before or after the writing.
        It is limited to 14 Hz.
        """
        if not self._sources:
            print "[EE] No sources."
            return
        if not isinstance(amount,int) or int(amount) < 0:
            print "[EE] Amount is less than zero."
            return
        if gen_random is None:
            gen_random = self._rand
        for i in xrange(amount):
            pre = time.time()
            for src in self._sources:
                clock_r = random.random() < 5.0
                if clock == 'pre' or (clock == 'rand' and clock_r):
                    src.clock()

                src.put_random() if gen_random else src.put_static()

                if clock == 'post' or (clock == 'rand' and not clock_r):
                    src.clock()

            sys.stdout.write(".")
            sys.stdout.flush()
            sleepy = 1.0/14 - (time.time()-pre)
            if sleepy > 0:
                time.sleep(sleepy)
        print ''

    def gen_reads(self, amount):
        """Generates readouts to simulate that the measuring device is turned off,
        e.g. in case of an MPS.
        """
        if not self._sources:
            print "[EE] No sources."
            return
        if not isinstance(amount,int) or int(amount) < 0:
            print "[EE] Amount is less than zero."
            return
        for i in xrange(amount):
            pre = time.time()
            [ src.clock() for src in self._sources ]
            sys.stdout.write("r")
            sys.stdout.flush()
            # Check that all clocks are finished with 5 s timeout.
            for j in range(500):
                if all([ src.clock_complete() for src in self._sources ]):
                    break
                time.sleep(0.01)
            else:
                print "[EE] Failed put!"
        print ''

    def set_mnpre(self, value):
        """Set the buffer length. Use this to limit the amount of RAM that is
        allocated.
        """
        if not self._sources:
            print "[EE] No sources."
            return
        if not isinstance(value,int) or int(value) < 1:
            print "[EE] NPRE is less than one."
            return
        for src in self._sources:
            src.mnpre = value

    def test_wo_trigger(self):
        """ULtra simple test. Push and Pop buffer."""
        self.gen_sim_data(30)

    def test_simple(self):
        """Simple test.
        This test ensures that all values can be popped, even though the buffer
        might not be filled yet. It also verifies that all MAXNPRE values can be
        popped if MAXNPRE values are pushed. Finally 20 values are also pushed to
        verify that the circular buffer is circular (MAXNPRE is 10).
        """
        for pre in [2,10,20]:
            for post in [2,10,20]:
                yield (self.fix_simple, 0, 10, pre, post, 'rand', 'rand')

    def test_poppush(self):
        """PUSH/POP test
        Test different order of PUSH/POP
        """
        cases = [('pre','pre'),('post','post'),('pre','post'),('post','pre'),('rand','rand')]
        for clk in cases:
            yield (self.fix_simple, 0, 25, 25, 25, clk[0], clk[1])

    def test_sets(self):
        """Multiple set tests"""
        for set in range(self._nbr_sets):
            yield (self.fix_simple, set, 25, 25, 25, 'rand', 'rand')

    def fix_simple(self, set, mnpre, pre, post, clk1='pre', clk2='pre'):
        if not self._sets[set]._sources:
            return
        self.set_mnpre(mnpre)
        s = self._sets[set]
        self.gen_sim_data(pre, True, clk1)
        s.trigger()
        self.gen_sim_data(post, True, clk2)
        self.gen_reads(max(pre,mnpre+4))
        res = s.verify(
                min(pre, s.npre, mnpre),
                min(post, s.npost))
        self._failed = not res
        assert res


    def test_unrelated(self):
        """Test unrelated samples.
        After a trigger, 'npost' samples should be marked for archiving. But if
        there is a long duration with no new data, we don't want to mark the
        samples for archiving.
        """
        for read in [5,10,15,20]:
            yield (self.fix_unrelated, read)

    def fix_unrelated(self, reads):
        self.gen_sim_data(20)
        self._sets[0].set_npost(reads)
        self._sets[0].trigger()
        self.gen_reads(reads+2)
        self.gen_sim_data(20)
        res = self._sets[0].verify(self._sets[0].npre, 0)
        self._failed = not res
        assert res


    def test_mnpre_w_content(self):
        """Test changing buffer size with content."""
        for content in [5,15]:
            for size in [5,15]:
                yield (self.fix_mnpre_w_content, content, size)

    def fix_mnpre_w_content(self, content, size):
        self.gen_sim_data(content)
        self.set_mnpre(size)
        self._sets[0].set_npre(size if size < content else content)
        self._sets[0].trigger()
        self.gen_reads(30)
        res = self._sets[0].verify(min(size,content), 0)
        self._failed = not res
        assert res

    def test_mnpre_empty(self):
        """Test changing buffer size without content."""
        for size in [5,15]:
            yield (self.fix_mnpre_empty, size)

    def fix_mnpre_empty(self, size):
        self.set_mnpre(10)
        self.set_mnpre(size)
        self._sets[0].set_npre(size)
        self.gen_sim_data(size)
        self._sets[0].trigger()
        self.gen_reads(30)
        res = self._sets[0].verify(size, 0)
        self._failed = not res
        assert res

    def test_multiple_trig_cont(self):
        for samples in [2,5,10]:
            for triggers in [3,5,10]:
               yield(self.fix_multiple_trig_cont, samples, triggers)

    def fix_multiple_trig_cont(self, size, n_triggers):
        """Test multiple triggers which should generate a continuous data feed"""
        s = self._sets[0]
        self.gen_sim_data(2*s.npre, True)
        assert size <= s.npre + s.npost
        for i in range(n_triggers):
            self.gen_sim_data(size, True)
            s.trigger()
        self.gen_sim_data(2*s.npost, True)
        self.gen_reads(self._sources[0].mnpre)
        res = s.verify_multi(s.npre + s.npost + size * (n_triggers-1), True)
        self._failed = not res
        assert res

    def test_multiple_trig_disc(self):
        for samples in [2,5,10]:
            for triggers in [3,5,10]:
               yield(self.fix_multiple_trig_disc, samples, triggers)

    def fix_multiple_trig_disc(self, size, n_triggers):
        """Test multiple triggers which should generate sets of raw data"""
        s = self._sets[0]
        self.gen_sim_data(2*s.npre, True)
        for i in range(n_triggers):
            self.gen_sim_data(size + s.npre + s.npost, True)
            s.trigger()
        self.gen_sim_data(2*s.npost, True)
        self.gen_reads(self._sources[0].mnpre)
        res = s.verify_multi((s.npre + s.npost)*n_triggers, False)
        self._failed = not res
        assert res

#    def test_simulation(self):
#        """Test with asynchronous multiple triggers during data acquisition."""
#        t = threading.Thread(target=self.gen_sim_data,args=(150,))
#        t.start()
#        s = self._sets[0]
#        n_triggers = 5
#        for i in range(n_triggers):
#            time.sleep(1+random.random())
#            s.trigger()
#        return s.verify_multi((s.npre + s.npost)*n_triggers, False)

@attr('slow')
class TestDodSlow(DodBase):
    def __init__(self):
        super(TestDodSlow, self).__init__("DT2", "dod-test", True)
        self._nelm = 240000
        self._nbr_sets = 3

    def connect_pvs(self):
        srcs = [[AiSource, 'DEV-AI', 1], [BiSource, 'DEV-BI', 1],
                [LonginSource, 'DEV-LONGIN', 1], [MbbiSource, 'DEV-MBBI', 1],
                [WaveformCharSource, 'DEV-WAVEFORM-C', self._nelm],
                [WaveformFloatSource, 'DEV-WAVEFORM-F', self._nelm],
                [WaveformDoubleSource, 'DEV-WAVEFORM-D', self._nelm]]
        self._sources = []
        for src in srcs:
            self._sources.append(src[0](src[1], src[2], self._prefix))
        self._sets = [ SourceSet('MPS', self._sources, 5, 5),
                       SourceSet('110', self._sources[0:3], 10, 10),
                       SourceSet('99',  self._sources[4:6], 10, 10) ]

@attr('fast')
class TestDodFast(DodBase):
    def __init__(self):
        super(TestDodFast, self).__init__("DT2", "dod-test", False)
        self._nbr_sets = 3

    def connect_pvs(self):
        self._sources = [AiSource('DEV-AI', 1, self._prefix)]
        self._sets = [ SourceSet('MPS', self._sources, 5, 5),
                       SourceSet('110', self._sources, 10, 10),
                       SourceSet('99',  [], 10, 10)]

@attr('slow', 'manywfs')
class TestDodManyWfsSlow(DodBase):
    def __init__(self):
        super(TestDodManyWfsSlow, self).__init__("DT3", "dod-manywfs", False)
        self._nelm   = 240000
        self._nbr_sets = 1

    def connect_pvs(self):
        self._sources = []
        for i in range(50):
            self._sources.append(WaveformFloatSource('WF%02d' % i, self._nelm, self._prefix))
        self._sets = [SourceSet('MPS', self._sources, 28, 10)]

@attr('fast', 'manywfs')
class TestDodManyWfsFast(DodBase):
    def __init__(self):
        super(TestDodManyWfsFast, self).__init__("DT3", "dod-manywfs", False)
        self._nelm   = 240000
        self._nbr_sets = 1

    def connect_pvs(self):
        self._sources = []
        for i in range(10):
            self._sources.append(WaveformFloatSource('WF%02d' % i, self._nelm, self._prefix))
        self._sets = [SourceSet('MPS', self._sources, 28, 10)]


if __name__ == "__main__":
    msg = """Instead of running this python script, set a "debug" attribute on the
    test you are running and run nosetests -a "debug"."""
    print(msg)
    #t = TestDodManyWfs()
    t = TestDodFast()
    #t = TestDodSlow()
    #t.connect_pvs()
    t.setUp()
    t.fix_multiple_trig_disc(2, 40)
    t.tearDown()
    t.setUp()
    t.fix_multiple_trig_disc(2, 40)
    t.tearDown()
    #t.fix_multiple_trig_disc(5, 3)
    #t.fix_simple(0, 50,50)
    #t.fix_simple(0, 2, 10, 'rand', 'rand')
    #t.fix_mnpre_empty(15)
    #t.fix_unrelated(5)
