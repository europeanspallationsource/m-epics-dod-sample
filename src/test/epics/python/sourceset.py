from datetime import datetime
from epics import PV
import time

class SourceSet(object):
    def __init__(self, name, sources, npre, npost):
        self._trigger = PV("IOC:DODSET{0}.PROC".format(name))
        self._sources = sources
        self.npre = npre
        self.npost = npost
        self._pvs = {}
        self._name = name

        for src in sources:
            self._pvs[id(src)] = (
                PV('{0}:DODSET{1}NPOST'.format(src._prefix, name)), 
                PV('{0}:DODSET{1}NPRE'.format(src._prefix, name)))

        self.set_npre(npre)
        self.set_npost(npost)

    def disconnect(self):
        for (key,pv) in self._pvs.iteritems():
            pv[0].disconnect()
            pv[1].disconnect()

    def get_dte_ts(self):
        return datetime.fromtimestamp(self._trigger.get_timevars()['timestamp'])

    def trigger(self):
        self._trigger.put(1, wait=True)

    def sources(self):
        return self._sources

    def set_npost(self, amount):
        [ self._pvs[id(src)][0].put(amount) for src in self._sources ]

    def set_npre(self, amount):
        [ self._pvs[id(src)][1].put(amount) for src in self._sources ]

    def verify(self, npre=None, npost=None):
        if not self._sources:
            print "[EE] No sources."
            return
        if npre is None:
            npre = set.npre
        if npost is None:
            npost = set.npost
        res = 0
        #time.sleep(0.2) # Make sure all data has arrived...
        dte_ts = self.get_dte_ts()
        for src in self.sources():
            err = src.verify(npre, npost, dte_ts)
            if err != 0:
                res = -1
        return res

    def verify_multi(self, size, continuous):
        if not self._sources:
            print "[EE] No sources."
            return
        res = 0
        for src in self.sources():
            err = src.verify_multi(size, continuous)
            if err != 0:
                res = -1
        return res

